﻿using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class TeamMember
{
    public string ID;
    public string Name;
    public string Role;
    public string Nickname;

}
