﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class HeaderScript : MonoBehaviour
{
    public GameObject title;
    public GameObject header;
    public TextAsset jsonFile;

    // Start is called before the first frame update
    void Start()
    {

        readJSONData();
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void readJSONData()
    {

        UIData uiDataInJson = JsonUtility.FromJson<UIData>(jsonFile.text);
        Debug.Log("The data UI is title: " + uiDataInJson.Title);
        title.GetComponent<UnityEngine.UI.Text>().text = uiDataInJson.Title;

        foreach (string headerText in uiDataInJson.ColumnHeaders)
        {
            //Debug.Log("and header: " + headerText);
            DataPersistence.Instance.headersArray.Add(headerText);
        }


        TeamMembers teamMembersInJson = JsonUtility.FromJson<TeamMembers>(jsonFile.text);
        foreach (TeamMember teamMember in teamMembersInJson.Data)
        {
            DataPersistence.Instance.membersArray.Add(teamMember);
        }
        
        Debug.Log("^^*^¿?¿ aHellow im team member count: " + DataPersistence.Instance.membersArray.Count);

        NotificationCenter.DefaultCenter.PostNotification(this, "OnDataLoaded");

        loadUIwithData();
    }


    private void loadUIwithData()
    {
        //Debug.Log(" *** The count is: " + DataPersistence.Instance.headersArray.Count);
        List<string> headerList = DataPersistence.Instance.headersArray;

        foreach (string headerText in headerList) { 
            GameObject newItem = Instantiate(header, transform);
            newItem.GetComponentInChildren<UnityEngine.UI.Text>().text = headerText;
            newItem.SetActive(true);
        }

    }
}
