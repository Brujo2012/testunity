﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataPersistence : Singleton<DataPersistence>
{
    // (Optional) Prevent non-singleton constructor use.
    protected DataPersistence() { }

    // Then add whatever code to the class you need as you normally would.
    public string title = "";
    public List <string> headersArray = new List<string>();
    public List <TeamMember> membersArray = new List<TeamMember>();
    
}

public class ItemsObject:  MonoBehaviour
{
    public List <string> itemArray = new List<string>();

}