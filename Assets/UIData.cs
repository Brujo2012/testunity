﻿using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class UIData
{
    public string Title;
    public string[] ColumnHeaders;
 
}

public class MemberData
{
    public List<Employee> Data = new List<Employee>();

}

public class Employee
{
    public List<string> Fields = new List<string>();

}
