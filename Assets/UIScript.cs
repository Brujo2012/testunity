﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScript : MonoBehaviour
{
    public GameObject rowItem;
    public GameObject rowField;

    // Start is called before the first frame update
    void Start()
    {
        NotificationCenter.DefaultCenter.AddObserver(this, "OnDataLoaded");
   
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnDataLoaded()
    {

        foreach (TeamMember teamMember in DataPersistence.Instance.membersArray)
        {

            GameObject parentItem = Instantiate<GameObject>(rowItem, transform);

                GameObject newItem1 = Instantiate<GameObject>(rowField, transform);
                newItem1.GetComponentInChildren<UnityEngine.UI.Text>().text = teamMember.ID;
                newItem1.SetActive(true);
                newItem1.transform.parent = parentItem.transform;

                GameObject newItem2 = Instantiate<GameObject>(rowField, transform);
                newItem2.GetComponentInChildren<UnityEngine.UI.Text>().text = teamMember.Name;
                newItem2.SetActive(true);
                newItem2.transform.parent = parentItem.transform;

                GameObject newItem3 = Instantiate<GameObject>(rowField, transform);
                newItem3.GetComponentInChildren<UnityEngine.UI.Text>().text = teamMember.Role;
                newItem3.SetActive(true);
                newItem3.transform.parent = parentItem.transform;

                GameObject newItem4 = Instantiate<GameObject>(rowField, transform);
                newItem4.GetComponentInChildren<UnityEngine.UI.Text>().text = teamMember.Nickname;
                newItem4.SetActive(true);
                newItem4.transform.parent = parentItem.transform;

            parentItem.SetActive(true);
        }

    }
}
